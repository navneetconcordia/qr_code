import React, { Component } from 'react'
import * as mockData from './MOCK_DATA.json';
import {
    Text,
    View,
    Button
  } from "react-native";
import QRCode from 'react-native-qrcode-svg';
export default class HomeScreen extends Component {
    state={
        person: 0
    }

    getRandomPerson=()=>{
        this.setState({
            person: Math.floor(Math.random() * 10)
        })
    }
    render() {
        const {person} = this.state;
        return (
            <View>
            <View style={{flexDirection:'row', alignItems: 'center', justifyContent: 'center', marginTop: 50}}>
            <QRCode
                size={300}
                value={JSON.stringify(mockData[person])}
            />
            </View>
            <View style={{flexDirection: 'row',justifyContent: 'center', paddingTop: 20}}>
                <Text>Scan this QR to get user's Information!</Text>
            </View>
            <View>
                <Button title={'GENERATE NEW QR'} onPress={()=>this.getRandomPerson()}/>
            </View>
            </View>
        )
    }
}
