import React, {Component} from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './HomeScreen';
import CameraStack from './CameraStack';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="QR-code" component={HomeScreen} />
        <Drawer.Screen name="Camera" component={CameraStack} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}