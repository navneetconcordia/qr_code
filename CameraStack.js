import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import CameraScreen from './CameraScreen';
import UserInfoScreen from './UserInfoScreen';

const Stack = createStackNavigator();

export default class CameraStack extends Component {

    state ={
        user: {}
    }
    getCameraInfo =(data) =>{
        if(data){
            this.props.navigation.navigate('User Info', {data})
        }
    }
    render() {
        return (
            <Stack.Navigator>
                <Stack.Screen name="Home" >
                {(props) => <CameraScreen {...props} getCameraInfo={this.getCameraInfo} />}
                </Stack.Screen>
                <Stack.Screen name="User Info" component={UserInfoScreen} />
            </Stack.Navigator>
        )
    }
}
