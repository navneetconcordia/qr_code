import React, { Component } from 'react'
import {View, Text} from 'react-native';
export default class UserInfoScreen extends Component {

    renderRow() {
        return (
            <View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                <View style={{ flex: 1, alignSelf: 'stretch' }} /> { /* Edit these as they are your cells. You may even take parameters to display different data / react elements etc. */}
                <View style={{ flex: 1, alignSelf: 'stretch' }} />
                <View style={{ flex: 1, alignSelf: 'stretch' }} />
                <View style={{ flex: 1, alignSelf: 'stretch' }} />
                <View style={{ flex: 1, alignSelf: 'stretch' }} />
            </View>
        );
    }

    render() {
        const data = this.props.route.params.data;
        console.log(data)
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {/* {
                data.map((datum) => { 
                    console.log(datum)
                    return this.renderRow();
                })
            } */}
            </View>
        )
    }
}
